# PRIMI PASSI

 1. Installare wildfly 18
 2. Installare IDE java (IntelliJ)
 3. Collegare e creare il DB MySQL 8
 4. Collegare il server all'IDE
 5. Scaricare il progetto
 6. Configurare il server in modo che si connetta al DB
 7. Nella cartella DB è presente il modello del database da avviare con workbench e generare il database
 7. Avviare il progetto

## Application server: Wildfly 18

### I passi da seguire sono i seguenti: 
- Aggiungere file di connector (Aggiungere nella cartella module il driver di mysql)
- Aggiungere modulo "module add --name=org.mysql --resources=mysql-connector-java-8.0.17.jar --dependencies=javax.api,javax.transaction.api"
- Lanciare "/subsystem=datasources/jdbc-driver=mysql:add(driver-module-name=org.mysql,driver-name=mysql,driver-class-name=com.mysql.jdbc.Driver)"
- Lanciare "/subsystem=datasources/data-source=unicam:add(jndi-name=java:/unicam, driver-name=mysql, driver-class=com.mysql.jdbc.Driver, connection-url=jdbc:mysql://ghomeserver.ddns.net:3306/develop,user-name=root,password=adminadmin)"
- Deploy del war generato con maven "mvn clean install"(Oppure direttamente da IDE)

### Principali cartelle con file temporanei:
 - WILDFLY_HOME\standalone\deployments 
 - project_folder\target
 
