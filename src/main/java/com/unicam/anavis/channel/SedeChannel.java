/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.channel;

import com.unicam.anavis.dao.SedeDao;
import com.unicam.anavis.mapper.SedeMapper;
import com.unicam.anavis.mapper.dto.SedeDto;
import com.unicam.log.LogInterceptor;
import com.unicam.util.ProfiledUser;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.Collection;


/**
 * @author mauro
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class SedeChannel extends BaseChannel {

    @EJB
    private SedeDao sedeDao;

    public String create(ProfiledUser profiledUser, SedeDto input) {
        return sedeDao.create(profiledUser, SedeMapper.dtoToEntity(input));
    }


    public Boolean edit(ProfiledUser profiledUser, String id, SedeDto i) {
        return sedeDao.edit(profiledUser, id, SedeMapper.dtoToEntity(i));
    }

    public void delete(ProfiledUser profiledUser, String id) {
        sedeDao.delete(profiledUser, id);
    }

    public SedeDto find(ProfiledUser profiledUser, String id) {
        return SedeMapper.entityToDto(sedeDao.find(profiledUser, id));
    }

    public Collection<SedeDto> findAll(ProfiledUser profiledUser) {
        return SedeMapper.listEntityToListDto(sedeDao.findAll(profiledUser));
    }

    public Boolean editSurvey(ProfiledUser profiledUser, String id, SedeDto sedeDto) {
        SedeDto sede = find(profiledUser, id);
        sede.setSurvey(sedeDto.getSurvey());
        sedeDao.edit(profiledUser, id, SedeMapper.dtoToEntity(sede));
        return true;
    }
}
