/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.channel;

import com.unicam.anavis.dao.StoricoDonazioneDao;
import com.unicam.anavis.dao.UtenteDao;
import com.unicam.anavis.mapper.StoricoDonazioneMapper;
import com.unicam.anavis.mapper.UtenteMapper;
import com.unicam.anavis.mapper.dto.StoricoDonazioneDto;
import com.unicam.log.LogInterceptor;
import com.unicam.util.ProfiledUser;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.Collection;


/**
 * @author mauro
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class StoricoDonazioneChannel extends BaseChannel {

    @EJB
    private StoricoDonazioneDao storicoDonazioneDao;
    @EJB
    private UtenteDao utenteDao;

    public String create(ProfiledUser profiledUser, StoricoDonazioneDto input) {
        return storicoDonazioneDao.create(profiledUser, StoricoDonazioneMapper.dtoToEntity(input));
    }


    public Boolean edit(ProfiledUser profiledUser, String id, StoricoDonazioneDto i) {
        return storicoDonazioneDao.edit(profiledUser, id, StoricoDonazioneMapper.dtoToEntity(i));
    }

    public void delete(ProfiledUser profiledUser, String id) {
        storicoDonazioneDao.delete(profiledUser, id);
    }


    public Collection<StoricoDonazioneDto> findAll(ProfiledUser profiledUser) {
        return StoricoDonazioneMapper.listEntityToListDto(storicoDonazioneDao.findByUser(profiledUser));
    }

    public Collection<StoricoDonazioneDto> findAll(ProfiledUser profiledUser, String sedeId) {
        Collection<StoricoDonazioneDto> temp = StoricoDonazioneMapper.listEntityToListDto(storicoDonazioneDao.findBySede(profiledUser, sedeId));
        for (StoricoDonazioneDto s :
                temp) {
            if (s.getUtenteId() != null) {
                s.setUtente(UtenteMapper.entityToDto(utenteDao.find(profiledUser, s.getUtenteId())));
            }
        }
        return temp;
    }

    public Collection<StoricoDonazioneDto> findAllWithUser(ProfiledUser profiledUser, String sedeId) {
        Collection<StoricoDonazioneDto> temp = StoricoDonazioneMapper.listEntityToListDto(storicoDonazioneDao.findBySede(profiledUser, sedeId));
        Collection<StoricoDonazioneDto> output = new ArrayList<>();
        for (StoricoDonazioneDto s :
                temp) {
            if (s.getUtenteId() != null) {
                s.setUtente(UtenteMapper.entityToDto(utenteDao.find(profiledUser, s.getUtenteId())));
                output.add(s);
            }
        }
        return output;
    }


}
