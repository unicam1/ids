/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.channel;

import com.unicam.anavis.dao.DipendenteDao;
import com.unicam.anavis.dao.SedeDao;
import com.unicam.anavis.dao.UtenteDao;
import com.unicam.anavis.mapper.SedeMapper;
import com.unicam.anavis.mapper.UtenteMapper;
import com.unicam.anavis.mapper.dto.UtenteDto;
import com.unicam.anavis.model.Dipendente;
import com.unicam.anavis.model.Utente;
import com.unicam.exception.Unicam4Exception;
import com.unicam.log.LogInterceptor;
import com.unicam.util.Constant;
import com.unicam.util.EncoderDecoder;
import com.unicam.util.ProfiledUser;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Collection;
import java.util.StringTokenizer;


/**
 * @author mauro
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class UserServiceChannel extends BaseChannel {

    @EJB
    private UtenteDao utenteDao;
    @EJB
    private DipendenteDao dipendenteDao;
    @EJB
    private SedeDao sedeDao;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String signIn(HttpServletRequest req, ProfiledUser profiledUser) throws Exception {
        // 1. Recupero l'authorization dall'header
        String auth = req.getHeader(Constant.AUTHORIZATION_PROPERTY);

        // 2. Recupero username e password encodate
        final String encodedUserPassword = auth.replaceFirst(Constant.AUTHENTICATION_SCHEME + " ", "");

        // 3. Faccio il decode da base64
        String usernameAndPassword = new String(Base64.getDecoder().decode(encodedUserPassword.getBytes()));

        // 4. Divido in username e password
        final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();

        // 5. Controllo l'account dell'utente
        Utente utente = utenteDao.login(profiledUser, username, password);

        // 6. Se l'utente può entrare salvo il fatto che qualcuno abbia provato ad entrare
        return EncoderDecoder.generateJwt(utente.getUsername(), profiledUser.getUuid(), req.getRemoteAddr(), utente.getId());
    }


    public String register(ProfiledUser profiledUser, UtenteDto utenteDto) throws Unicam4Exception {
        return utenteDao.create(profiledUser, UtenteMapper.dtoToEntity(utenteDto));
    }
    public Boolean edit(ProfiledUser profiledUser, UtenteDto utenteDto, String id) throws Unicam4Exception {
        return utenteDao.edit(profiledUser,id, UtenteMapper.dtoToEntity(utenteDto));
    }

    public Collection<Utente> findAll(ProfiledUser profiledUser, String idSede, String role) {
        return utenteDao.findAll(profiledUser, idSede, role);
    }

    public String register(ProfiledUser profiledUser, UtenteDto utenteDto, String idSede) throws Unicam4Exception {
        String userId = utenteDao.create(profiledUser, UtenteMapper.dtoToEntity(utenteDto));
        Dipendente input = new Dipendente();
        input.setUtenteId(userId);
        input.setSedeIdSede(idSede);
        dipendenteDao.create(profiledUser, input);
        return userId;
    }

    public UtenteDto myInfo(ProfiledUser profiledUser) throws Unicam4Exception {
        UtenteDto output = UtenteMapper.entityToDto(utenteDao.find(profiledUser, profiledUser.getUserId()));
        output.setSede(SedeMapper.entityToDto(sedeDao.findByUser(profiledUser)));
        return output;
    }
}
