/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.dao;

import com.unicam.util.ProfiledUser;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @param <T>
 * @author mauro
 */
public class BaseDao<T> {

    protected static final String PERSISTENCE_UNIT_NAME = "unicamUnit";
    private static final Logger LOG = Logger.getLogger(BaseDao.class.getName());
    protected static EntityManagerFactory factory;

    private String setId(T entity) {
        String newUuid = UUID.randomUUID().toString();
        try {
            Method m;
            m = entity.getClass().getMethod("setId", String.class);
            m.invoke(entity, newUuid);
            return newUuid;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error creating new ID", e);
        }
        return newUuid;
    }


    protected EntityManager getEntityManager() {
        if (factory != null && factory.isOpen()) {
            return factory.createEntityManager();
        }
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        return em;
    }

    protected String create(T entity, ProfiledUser profiledUser) {
        String uuid = setId(entity);
        createWithoutId(entity, profiledUser);
        return uuid;
    }

    protected void createWithoutId(T entity, ProfiledUser profiledUser) {
        getEntityManager().persist(entity);
    }

    protected void edit(T entity) {
        EntityManager e = getEntityManager();
        e.getTransaction().begin();
        getEntityManager().merge(entity);
        e.getTransaction().commit();
    }

    protected void delete(Class<T> entityClass, Object id) {
        EntityManager e = getEntityManager();
        e.remove(e.find(entityClass, id));
    }

    protected T find(Object id, Class<T> entityClass) {
        return getEntityManager().find(entityClass, id);
    }

    protected List<T> findAll(Class<T> entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    protected List<T> findRange(int[] range, Class<T> entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    protected int count(Class<T> entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }


}
