/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.dao;

import com.unicam.anavis.model.Dipendente;
import com.unicam.anavis.model.Utente;
import com.unicam.exception.Unicam4Exception;
import com.unicam.log.LogInterceptor;
import com.unicam.util.Constant;
import com.unicam.util.ErrorConst;
import com.unicam.util.PasswordUtils;
import com.unicam.util.ProfiledUser;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * @author mauro
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class DipendenteDao extends BaseDao<Dipendente> {

    public String create(ProfiledUser profiledUser, Dipendente entity) throws Unicam4Exception {
        // 1. Preparo l'inserimento dell'utente
        return super.create(entity, profiledUser);
    }
}
