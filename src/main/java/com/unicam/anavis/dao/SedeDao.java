/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.dao;

import com.unicam.anavis.model.Sede;
import com.unicam.log.LogInterceptor;
import com.unicam.util.ProfiledUser;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * @author mauro
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class SedeDao extends BaseDao<Sede> {

    public String create(ProfiledUser profiledUser, Sede entity) {
        return super.create(entity, profiledUser);
    }


    public Boolean edit(ProfiledUser profiledUser, String id, Sede i) {
        EntityManager em = getEntityManager();
        // 0. Recupero il personal data
        Sede output = em.find(Sede.class, id);
        em.getTransaction().begin();

        // 1. Aggiorno i campi
        output.setId(i.getId());
        output.setNomeSede(i.getNomeSede());
        output.setIndirizzoSede(i.getIndirizzoSede());
        output.setCapSede(i.getCapSede());
        output.setProvSede(i.getProvSede());
        output.setDescrizione(i.getDescrizione());
        output.setTelSede(i.getTelSede());
        output.setEmail(i.getEmail());
        output.setSurvey(i.getSurvey());
        output.setEmergenza(i.getEmergenza());

        em.getTransaction().commit();
        return true;
    }

    public void delete(ProfiledUser profiledUser, String id) {
        super.delete(Sede.class, id);
    }

    public Sede find(ProfiledUser profiledUser, String id) {
        return super.find(id, Sede.class);
    }

    public Collection<Sede> findAll(ProfiledUser profiledUser) {
        return super.findAll(Sede.class);
    }

    public Sede findByUser(ProfiledUser profiledUser) {
        EntityManager em = getEntityManager();
        TypedQuery<Sede> q = em.createNamedQuery("Sede.findByUser", Sede.class)
                .setParameter("id", profiledUser.getUserId()).setMaxResults(1);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
