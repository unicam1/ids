/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.dao;

import com.unicam.anavis.model.StoricoDonazione;
import com.unicam.log.LogInterceptor;
import com.unicam.util.ProfiledUser;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * @author mauro
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class StoricoDonazioneDao extends BaseDao<StoricoDonazione> {

    public String create(ProfiledUser profiledUser, StoricoDonazione entity) {
        return super.create(entity, profiledUser);
    }


    public Boolean edit(ProfiledUser profiledUser, String id, StoricoDonazione i) {
        EntityManager em = getEntityManager();
        // 0. Recupero il personal data
        StoricoDonazione output = em.find(StoricoDonazione.class, id);
        em.getTransaction().begin();

        // 1. Aggiorno i campi
        output.setGruppoSanguinio(i.getGruppoSanguinio());
        output.setDataDepositoSangue(i.getDataDepositoSangue());
        output.setPatologie(i.getPatologie());
        output.setDescrizioneAnomalie(i.getDescrizioneAnomalie());
        output.setLvGlobuli(i.getLvGlobuli());
        output.setLvPiastrine(i.getLvPiastrine());
        output.setQuantitaSangueDonato(i.getQuantitaSangueDonato());
        output.setRisposteQuestionario(i.getRisposteQuestionario());
        output.setUtenteId(i.getUtenteId());
        output.setTime(i.getTime());
        output.setDuration(i.getDuration());
        em.getTransaction().commit();
        return true;
    }

    public void delete(ProfiledUser profiledUser, String id) {
        super.delete(StoricoDonazione.class, id);
    }

    public Collection<StoricoDonazione> findAll(ProfiledUser profiledUser) {
        return super.findAll(StoricoDonazione.class);
    }

    public Collection<StoricoDonazione> findByUser(ProfiledUser profiledUser) {
        EntityManager em = getEntityManager();
        TypedQuery<StoricoDonazione> q = em.createNamedQuery("StoricoDonazione.findByUtenteId", StoricoDonazione.class)
                .setParameter("id", profiledUser.getUserId());
        return q.getResultList();
    }

    public Collection<StoricoDonazione> findBySede(ProfiledUser profiledUser, String sedeId) {
        EntityManager em = getEntityManager();
        TypedQuery<StoricoDonazione> q = em.createNamedQuery("StoricoDonazione.findBySedeId", StoricoDonazione.class)
                .setParameter("id", sedeId);
        return q.getResultList();
    }
}
