/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.dao;

import com.unicam.anavis.model.Utente;
import com.unicam.exception.Unicam4Exception;
import com.unicam.log.LogInterceptor;
import com.unicam.util.Constant;
import com.unicam.util.ErrorConst;
import com.unicam.util.PasswordUtils;
import com.unicam.util.ProfiledUser;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * @author mauro
 */
@Stateless
@Interceptors(LogInterceptor.class)
public class UtenteDao extends BaseDao<Utente> {

    public String create(ProfiledUser profiledUser, Utente entity) throws Unicam4Exception {
        // 1. Preparo l'inserimento dell'utente
        checkUsername(profiledUser, entity);
        String password = entity.getPassword();
        String salt = PasswordUtils.getSalt(Constant.SALT_LENG);
        String hashedPassword = PasswordUtils.generateSecurePassword(password, salt);
        entity.setSalt(salt);
        entity.setPassword(hashedPassword);
        return super.create(entity, profiledUser);
    }

    private void checkUsername(ProfiledUser profiledUser, Utente entity) throws Unicam4Exception {
        try {
            Utente customer;
            EntityManager em = getEntityManager();
            TypedQuery<Utente> q = em.createNamedQuery("Utente.findByUsername", Utente.class)
                    .setParameter("id", entity.getUsername()).setMaxResults(1);
            // 1. Controllo il primo risultato, se è false allora non esiste l'utente
            customer = q.getSingleResult();
            if (customer != null) {
                throw new Unicam4Exception("Username existing", profiledUser, ErrorConst.ERROR_AUTH_UNAUTH);
            }
        } catch (NoResultException e) {
            //Va bene non devo trovarne
        }
    }

    public Utente login(ProfiledUser profiledUser, String username, String password)
            throws Exception {
        EntityManager em = getEntityManager();
        Boolean check = false;
        Utente customer;
        try {
            TypedQuery<Utente> q = em.createNamedQuery("Utente.findByUsername", Utente.class)
                    .setParameter("id", username).setMaxResults(1);
            // 1. Controllo il primo risultato, se è false allora non esiste l'utente
            customer = q.getSingleResult();
            // 2. Recupero l'hash della password
            String securedPassword = customer.getPassword();
            // 3. Il relativo salt
            String salt = customer.getSalt();
            check = PasswordUtils.verifyUserPassword(password, securedPassword, salt);
            if (check == false) {
                throw new Unicam4Exception("Unauthorized user, password not match", profiledUser, ErrorConst.ERROR_AUTH_UNAUTH);
            }
        } catch (NoResultException e) {
            throw new Unicam4Exception("Unauthorized user, username not found", profiledUser, ErrorConst.ERROR_AUTH_UNAUTH);
        } catch (Exception e) {
            throw e;
        }
        return customer;
    }

    public Boolean edit(ProfiledUser profiledUser, String id, Utente input) {
        EntityManager em = getEntityManager();
        // 0. Recupero il personal data
        Utente utente = em.find(Utente.class, id);
        em.getTransaction().begin();

        // 1. Aggiorno i campi
        utente.setIdentifierCode(input.getIdentifierCode());
        utente.setName(input.getName());
        utente.setEmail(input.getEmail());
        utente.setCognome(input.getCognome());
        utente.setGenere(input.getGenere());
        utente.setDataNascita(input.getDataNascita());
        utente.setCittaNascita(input.getCittaNascita());
        utente.setProvinciaNascita(input.getProvinciaNascita());
        utente.setStatoNascita(input.getStatoNascita());
        utente.setCitta(input.getCitta());
        utente.setProvincia(input.getProvincia());
        utente.setStato(input.getStato());
        utente.setNumeroTelefono(input.getNumeroTelefono());
        utente.setNumeroTelefonoAlternativo(input.getNumeroTelefonoAlternativo());
        utente.setIndirizzo(input.getIndirizzo());
        utente.setNumeroCivico(input.getNumeroCivico());
        utente.setCap(input.getCap());
        utente.setStatoDomicilio(input.getStatoDomicilio());
        utente.setProvinciaDomicilio(input.getProvinciaDomicilio());
        utente.setCittaDomicilio(input.getCittaDomicilio());
        utente.setIndirizzoDomicilio(input.getIndirizzoDomicilio());
        utente.setNumeroCivicoDomicilio(input.getNumeroCivicoDomicilio());
        utente.setCapDomicilio(input.getCapDomicilio());
        utente.setId(input.getId());
        utente.setUsername(input.getUsername());
        utente.setPassword(input.getPassword());
        utente.setRuolo(input.getRuolo());
        utente.setStatoUtente(input.getStatoUtente());

        em.getTransaction().commit();
        return true;
    }

    public void delete(ProfiledUser profiledUser, String id) {
        super.delete(Utente.class, id);
    }

    public Utente find(ProfiledUser profiledUser, String id) {
        return (Utente) super.find(id, Utente.class);
    }

    public Collection<Utente> findAll(ProfiledUser profiledUser, String idSede, String ruolo) {
        EntityManager em = getEntityManager();
        TypedQuery<Utente> q = em.createNamedQuery("Utente.findBySedeRuolo", Utente.class)
                .setParameter("idSede", idSede)
                .setParameter("ruolo", ruolo);
        return q.getResultList();
    }

}
