package com.unicam.anavis.mapper;

import com.unicam.anavis.mapper.dto.SedeDto;
import com.unicam.anavis.model.Sede;

import java.util.ArrayList;
import java.util.Collection;

public class SedeMapper extends BaseMapper {

    public static Sede dtoToEntity(SedeDto i) {
        Sede output = new Sede();
        output.setId(i.getIdSede());
        output.setNomeSede(i.getNomeSede());
        output.setIndirizzoSede(i.getIndirizzoSede());
        output.setCapSede(i.getCapSede());
        output.setProvSede(i.getProvSede());
        output.setDescrizione(i.getDescrizione());
        output.setTelSede(i.getTelSede());
        output.setEmail(i.getEmail());
        output.setSurvey(i.getSurvey());
        output.setEmergenza(i.getEmergenza());
        return output;
    }

    public static SedeDto entityToDto(Sede i) {
        if (i == null) return null;
        SedeDto output = new SedeDto();
        output.setIdSede(i.getId());
        output.setNomeSede(i.getNomeSede());
        output.setIndirizzoSede(i.getIndirizzoSede());
        output.setCapSede(i.getCapSede());
        output.setProvSede(i.getProvSede());
        output.setDescrizione(i.getDescrizione());
        output.setTelSede(i.getTelSede());
        output.setEmail(i.getEmail());
        output.setSurvey(i.getSurvey());
        output.setEmergenza(i.getEmergenza());
        return output;
    }

    public static Collection<Sede> listDtoToListEntity(Collection<SedeDto> i) {
        Collection<Sede> output = new ArrayList<Sede>();
        for (SedeDto e : i) {
            output.add(dtoToEntity(e));
        }
        return output;
    }

    public static Collection<SedeDto> listEntityToListDto(Collection<Sede> i) {
        Collection<SedeDto> output = new ArrayList<SedeDto>();
        for (Sede e : i) {
            output.add(entityToDto(e));
        }
        return output;
    }

}
