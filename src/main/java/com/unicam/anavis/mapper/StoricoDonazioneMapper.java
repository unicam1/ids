package com.unicam.anavis.mapper;

import com.unicam.anavis.mapper.dto.StoricoDonazioneDto;
import com.unicam.anavis.model.StoricoDonazione;

import java.util.ArrayList;
import java.util.Collection;

public class StoricoDonazioneMapper extends BaseMapper {

    public static StoricoDonazione dtoToEntity(StoricoDonazioneDto i) {
        StoricoDonazione output = new StoricoDonazione();
        output.setId(i.getIdStorico());
        output.setGruppoSanguinio(i.getGruppoSanguinio());
        output.setDataDepositoSangue(i.getDataDepositoSangue());
        output.setPatologie(i.getPatologie());
        output.setDescrizioneAnomalie(i.getDescrizioneAnomalie());
        output.setLvGlobuli(i.getLvGlobuli());
        output.setRisposteQuestionario(i.getRisposteQuestionario());
        output.setLvPiastrine(i.getLvPiastrine());
        output.setQuantitaSangueDonato(i.getQuantitaSangueDonato());
        output.setUtenteId(i.getUtenteId());
        output.setSedeIdSede(i.getSedeIdSede());
        output.setTime(i.getTime());
        output.setDuration(i.getDuration());


        return output;
    }

    public static StoricoDonazioneDto entityToDto(StoricoDonazione i) {
        StoricoDonazioneDto output = new StoricoDonazioneDto();
        output.setIdStorico(i.getId());
        output.setGruppoSanguinio(i.getGruppoSanguinio());
        output.setDataDepositoSangue(i.getDataDepositoSangue());
        output.setPatologie(i.getPatologie());
        output.setDescrizioneAnomalie(i.getDescrizioneAnomalie());
        output.setLvGlobuli(i.getLvGlobuli());
        output.setLvPiastrine(i.getLvPiastrine());
        output.setQuantitaSangueDonato(i.getQuantitaSangueDonato());
        output.setUtenteId(i.getUtenteId());
        output.setSedeIdSede(i.getSedeIdSede());
        output.setRisposteQuestionario(i.getRisposteQuestionario());

        output.setTime(i.getTime());
        output.setDuration(i.getDuration());
        return output;
    }

    public static Collection<StoricoDonazione> listDtoToListEntity(Collection<StoricoDonazioneDto> i) {
        Collection<StoricoDonazione> output = new ArrayList<StoricoDonazione>();
        for (StoricoDonazioneDto e : i) {
            output.add(dtoToEntity(e));
        }
        return output;
    }

    public static Collection<StoricoDonazioneDto> listEntityToListDto(Collection<StoricoDonazione> i) {
        Collection<StoricoDonazioneDto> output = new ArrayList<StoricoDonazioneDto>();
        for (StoricoDonazione e : i) {
            output.add(entityToDto(e));
        }
        return output;
    }

}
