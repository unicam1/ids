package com.unicam.anavis.mapper;

import com.unicam.anavis.mapper.dto.UtenteDto;
import com.unicam.anavis.model.Utente;

import java.util.ArrayList;
import java.util.Collection;

public class UtenteMapper extends BaseMapper {

    public static Utente dtoToEntity(UtenteDto input) {
        Utente output = new Utente();
        output.setIdentifierCode(input.getIdentifierCode());
        output.setName(input.getName());
        output.setEmail(input.getEmail());
        output.setCognome(input.getCognome());
        output.setGenere(input.getGenere());
        output.setDataNascita(input.getDataNascita());
        output.setCittaNascita(input.getCittaNascita());
        output.setProvinciaNascita(input.getProvinciaNascita());
        output.setStatoNascita(input.getStatoNascita());
        output.setCitta(input.getCitta());
        output.setProvincia(input.getProvincia());
        output.setStato(input.getStato());
        output.setNumeroTelefono(input.getNumeroTelefono());
        output.setNumeroTelefonoAlternativo(input.getNumeroTelefonoAlternativo());
        output.setIndirizzo(input.getIndirizzo());
        output.setNumeroCivico(input.getNumeroCivico());
        output.setCap(input.getCap());
        output.setStatoDomicilio(input.getStatoDomicilio());
        output.setProvinciaDomicilio(input.getProvinciaDomicilio());
        output.setCittaDomicilio(input.getCittaDomicilio());
        output.setIndirizzoDomicilio(input.getIndirizzoDomicilio());
        output.setNumeroCivicoDomicilio(input.getNumeroCivicoDomicilio());
        output.setCapDomicilio(input.getCapDomicilio());
        output.setId(input.getId());
        output.setUsername(input.getUsername());
        output.setPassword(input.getPassword());
        output.setRuolo(input.getRuolo());
        output.setStatoUtente(input.getStatoUtente());

        return output;
    }

    public static UtenteDto entityToDto(Utente input) {
        UtenteDto output = new UtenteDto();
        output.setIdentifierCode(input.getIdentifierCode());
        output.setName(input.getName());
        output.setEmail(input.getEmail());
        output.setCognome(input.getCognome());
        output.setGenere(input.getGenere());
        output.setDataNascita(input.getDataNascita());
        output.setCittaNascita(input.getCittaNascita());
        output.setProvinciaNascita(input.getProvinciaNascita());
        output.setStatoNascita(input.getStatoNascita());
        output.setCitta(input.getCitta());
        output.setProvincia(input.getProvincia());
        output.setStato(input.getStato());
        output.setNumeroTelefono(input.getNumeroTelefono());
        output.setNumeroTelefonoAlternativo(input.getNumeroTelefonoAlternativo());
        output.setIndirizzo(input.getIndirizzo());
        output.setNumeroCivico(input.getNumeroCivico());
        output.setCap(input.getCap());
        output.setStatoDomicilio(input.getStatoDomicilio());
        output.setProvinciaDomicilio(input.getProvinciaDomicilio());
        output.setCittaDomicilio(input.getCittaDomicilio());
        output.setIndirizzoDomicilio(input.getIndirizzoDomicilio());
        output.setNumeroCivicoDomicilio(input.getNumeroCivicoDomicilio());
        output.setCapDomicilio(input.getCapDomicilio());
        output.setId(input.getId());
        output.setUsername(input.getUsername());
        output.setPassword(input.getPassword());
        output.setRuolo(input.getRuolo());
        output.setStatoUtente(input.getStatoUtente());
        return output;
    }

    public static Collection<Utente> listDtoToListEntity(Collection<UtenteDto> i) {
        Collection<Utente> output = new ArrayList<Utente>();
        for (UtenteDto e : i) {
            output.add(dtoToEntity(e));
        }
        return output;
    }

    public static Collection<UtenteDto> listEntityToListDto(Collection<Utente> i) {
        Collection<UtenteDto> output = new ArrayList<UtenteDto>();
        for (Utente e : i) {
            output.add(entityToDto(e));
        }
        return output;
    }

}
