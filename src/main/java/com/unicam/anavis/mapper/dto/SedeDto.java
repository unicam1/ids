package com.unicam.anavis.mapper.dto;

import java.io.Serializable;
import java.util.Set;

public class SedeDto implements Serializable {
    private Set<UtenteDto> utenti;
    private String idSede;
    private String nomeSede;
    private String indirizzoSede;
    private String capSede;
    private String provSede;
    private String descrizione;
    private int telSede;
    private String email;
    private String survey;
    private String emergenza;

    public String getSurvey() {
        return survey;
    }

    public void setSurvey(String survey) {
        this.survey = survey;
    }

    public Set<UtenteDto> getUtenti() {
        return utenti;
    }

    public void setUtenti(Set<UtenteDto> utenti) {
        this.utenti = utenti;
    }

    public String getIdSede() {
        return idSede;
    }

    public void setIdSede(String idSede) {
        this.idSede = idSede;
    }

    public String getNomeSede() {
        return nomeSede;
    }

    public void setNomeSede(String nomeSede) {
        this.nomeSede = nomeSede;
    }

    public String getIndirizzoSede() {
        return indirizzoSede;
    }

    public void setIndirizzoSede(String indirizzoSede) {
        this.indirizzoSede = indirizzoSede;
    }

    public String getCapSede() {
        return capSede;
    }

    public void setCapSede(String capSede) {
        this.capSede = capSede;
    }

    public String getProvSede() {
        return provSede;
    }

    public void setProvSede(String provSede) {
        this.provSede = provSede;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public int getTelSede() {
        return telSede;
    }

    public void setTelSede(int telSede) {
        this.telSede = telSede;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmergenza() {
        return emergenza;
    }

    public void setEmergenza(String emergenza) {
        this.emergenza = emergenza;
    }
}