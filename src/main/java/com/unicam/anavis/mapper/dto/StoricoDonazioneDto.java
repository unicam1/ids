package com.unicam.anavis.mapper.dto;

import java.util.Date;

public class StoricoDonazioneDto {
    private String idStorico;
    private String gruppoSanguinio;
    private String dataDepositoSangue;
    private String patologie;
    private String descrizioneAnomalie;
    private String lvGlobuli;
    private String lvPiastrine;
    private String quantitaSangueDonato;
    private String utenteId;
    private UtenteDto utente;
    private String sedeIdSede;
    private Date time;
    private Integer duration;
    private String risposteQuestionario;

    public String getRisposteQuestionario() {
        return risposteQuestionario;
    }

    public void setRisposteQuestionario(String risposteQuestionario) {
        this.risposteQuestionario = risposteQuestionario;
    }

    public UtenteDto getUtente() {
        return utente;
    }

    public void setUtente(UtenteDto utente) {
        this.utente = utente;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getIdStorico() {
        return idStorico;
    }

    public void setIdStorico(String idStorico) {
        this.idStorico = idStorico;
    }

    public String getGruppoSanguinio() {
        return gruppoSanguinio;
    }

    public void setGruppoSanguinio(String gruppoSanguinio) {
        this.gruppoSanguinio = gruppoSanguinio;
    }

    public String getDataDepositoSangue() {
        return dataDepositoSangue;
    }

    public void setDataDepositoSangue(String dataDepositoSangue) {
        this.dataDepositoSangue = dataDepositoSangue;
    }

    public String getPatologie() {
        return patologie;
    }

    public void setPatologie(String patologie) {
        this.patologie = patologie;
    }

    public String getDescrizioneAnomalie() {
        return descrizioneAnomalie;
    }

    public void setDescrizioneAnomalie(String descrizioneAnomalie) {
        this.descrizioneAnomalie = descrizioneAnomalie;
    }

    public String getLvGlobuli() {
        return lvGlobuli;
    }

    public void setLvGlobuli(String lvGlobuli) {
        this.lvGlobuli = lvGlobuli;
    }

    public String getLvPiastrine() {
        return lvPiastrine;
    }

    public void setLvPiastrine(String lvPiastrine) {
        this.lvPiastrine = lvPiastrine;
    }

    public String getQuantitaSangueDonato() {
        return quantitaSangueDonato;
    }

    public void setQuantitaSangueDonato(String quantitaSangueDonato) {
        this.quantitaSangueDonato = quantitaSangueDonato;
    }

    public String getUtenteId() {
        return utenteId;
    }

    public void setUtenteId(String utenteId) {
        this.utenteId = utenteId;
    }

    public String getSedeIdSede() {
        return sedeIdSede;
    }

    public void setSedeIdSede(String sedeIdSede) {
        this.sedeIdSede = sedeIdSede;
    }
}
