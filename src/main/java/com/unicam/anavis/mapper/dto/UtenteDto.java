package com.unicam.anavis.mapper.dto;

import com.unicam.anavis.model.StoricoDonazione;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

public class UtenteDto implements Serializable {
    private String identifierCode;
    private String name;
    private String email;
    private String cognome;
    private String genere;
    private Date dataNascita;
    private String cittaNascita;
    private String provinciaNascita;
    private String statoNascita;
    private String citta;
    private String provincia;
    private String stato;
    private String numeroTelefono;
    private String numeroTelefonoAlternativo;
    private String indirizzo;
    private String numeroCivico;
    private String cap;
    private String statoDomicilio;
    private String provinciaDomicilio;
    private String cittaDomicilio;
    private String indirizzoDomicilio;
    private String numeroCivicoDomicilio;
    private String capDomicilio;
    private String id;
    private String username;
    private String password;
    private String salt;
    private String ruolo;
    private Integer statoUtente;
    private Collection<StoricoDonazione> storicoDonazionesById;
    private SedeDto sede;

    public SedeDto getSede() {
        return sede;
    }

    public void setSede(SedeDto sede) {
        this.sede = sede;
    }

    public String getIdentifierCode() {
        return identifierCode;
    }

    public void setIdentifierCode(String identifierCode) {
        this.identifierCode = identifierCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getCittaNascita() {
        return cittaNascita;
    }

    public void setCittaNascita(String cittaNascita) {
        this.cittaNascita = cittaNascita;
    }

    public String getProvinciaNascita() {
        return provinciaNascita;
    }

    public void setProvinciaNascita(String provinciaNascita) {
        this.provinciaNascita = provinciaNascita;
    }

    public String getStatoNascita() {
        return statoNascita;
    }

    public void setStatoNascita(String statoNascita) {
        this.statoNascita = statoNascita;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getNumeroTelefonoAlternativo() {
        return numeroTelefonoAlternativo;
    }

    public void setNumeroTelefonoAlternativo(String numeroTelefonoAlternativo) {
        this.numeroTelefonoAlternativo = numeroTelefonoAlternativo;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getNumeroCivico() {
        return numeroCivico;
    }

    public void setNumeroCivico(String numeroCivico) {
        this.numeroCivico = numeroCivico;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getStatoDomicilio() {
        return statoDomicilio;
    }

    public void setStatoDomicilio(String statoDomicilio) {
        this.statoDomicilio = statoDomicilio;
    }

    public String getProvinciaDomicilio() {
        return provinciaDomicilio;
    }

    public void setProvinciaDomicilio(String provinciaDomicilio) {
        this.provinciaDomicilio = provinciaDomicilio;
    }

    public String getCittaDomicilio() {
        return cittaDomicilio;
    }

    public void setCittaDomicilio(String cittaDomicilio) {
        this.cittaDomicilio = cittaDomicilio;
    }

    public String getIndirizzoDomicilio() {
        return indirizzoDomicilio;
    }

    public void setIndirizzoDomicilio(String indirizzoDomicilio) {
        this.indirizzoDomicilio = indirizzoDomicilio;
    }

    public String getNumeroCivicoDomicilio() {
        return numeroCivicoDomicilio;
    }

    public void setNumeroCivicoDomicilio(String numeroCivicoDomicilio) {
        this.numeroCivicoDomicilio = numeroCivicoDomicilio;
    }

    public String getCapDomicilio() {
        return capDomicilio;
    }

    public void setCapDomicilio(String capDomicilio) {
        this.capDomicilio = capDomicilio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public Integer getStatoUtente() {
        return statoUtente;
    }

    public void setStatoUtente(Integer statoUtente) {
        this.statoUtente = statoUtente;
    }

    public Collection<StoricoDonazione> getStoricoDonazionesById() {
        return storicoDonazionesById;
    }

    public void setStoricoDonazionesById(Collection<StoricoDonazione> storicoDonazionesById) {
        this.storicoDonazionesById = storicoDonazionesById;
    }
}