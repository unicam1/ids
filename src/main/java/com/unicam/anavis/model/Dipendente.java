package com.unicam.anavis.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.Objects;

@Entity
@IdClass(DipendentePK.class)
public class Dipendente {
    private String sedeIdSede;
    private String utenteId;

    @Id
    @Column(name = "sede_ID_Sede")
    public String getSedeIdSede() {
        return sedeIdSede;
    }

    public void setSedeIdSede(String sedeIdSede) {
        this.sedeIdSede = sedeIdSede;
    }

    @Id
    @Column(name = "UTENTE_ID")
    public String getUtenteId() {
        return utenteId;
    }

    public void setUtenteId(String utenteId) {
        this.utenteId = utenteId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dipendente that = (Dipendente) o;
        return Objects.equals(sedeIdSede, that.sedeIdSede) &&
                Objects.equals(utenteId, that.utenteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sedeIdSede, utenteId);
    }
}
