package com.unicam.anavis.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class DipendentePK implements Serializable {
    private String sedeIdSede;
    private String utenteId;

    @Column(name = "sede_ID_Sede")
    @Id
    public String getSedeIdSede() {
        return sedeIdSede;
    }

    public void setSedeIdSede(String sedeIdSede) {
        this.sedeIdSede = sedeIdSede;
    }

    @Column(name = "UTENTE_ID")
    @Id
    public String getUtenteId() {
        return utenteId;
    }

    public void setUtenteId(String utenteId) {
        this.utenteId = utenteId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DipendentePK that = (DipendentePK) o;
        return Objects.equals(sedeIdSede, that.sedeIdSede) &&
                Objects.equals(utenteId, that.utenteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sedeIdSede, utenteId);
    }
}
