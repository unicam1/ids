package com.unicam.anavis.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "modello_questionario", schema = "uni_project_ids", catalog = "")
public class ModelloQuestionario {
    private String id;
    private Timestamp ultimoAggiornamento;
    private String pathFile;
    private String sedeIdSede;

    @Id
    @Column(name = "id_modello_questionario")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ultimo_aggiornamento")
    public Timestamp getUltimoAggiornamento() {
        return ultimoAggiornamento;
    }

    public void setUltimoAggiornamento(Timestamp ultimoAggiornamento) {
        this.ultimoAggiornamento = ultimoAggiornamento;
    }

    @Basic
    @Column(name = "path_file")
    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    @Basic
    @Column(name = "sede_ID_Sede")
    public String getSedeIdSede() {
        return sedeIdSede;
    }

    public void setSedeIdSede(String sedeIdSede) {
        this.sedeIdSede = sedeIdSede;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ModelloQuestionario that = (ModelloQuestionario) o;

        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;
        if (ultimoAggiornamento != null ? !ultimoAggiornamento.equals(that.ultimoAggiornamento) : that.ultimoAggiornamento != null)
            return false;
        if (pathFile != null ? !pathFile.equals(that.pathFile) : that.pathFile != null) return false;
        if (sedeIdSede != null ? !sedeIdSede.equals(that.sedeIdSede) : that.sedeIdSede != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ultimoAggiornamento != null ? ultimoAggiornamento.hashCode() : 0);
        result = 31 * result + (pathFile != null ? pathFile.hashCode() : 0);
        result = 31 * result + (sedeIdSede != null ? sedeIdSede.hashCode() : 0);
        return result;
    }
}
