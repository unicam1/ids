package com.unicam.anavis.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name = "Sede.findByUser", query = "SELECT u FROM Sede u, Dipendente d WHERE d.utenteId = :id AND d.sedeIdSede = u.id"),
})
public class Sede {
    private String id;
    private String nomeSede;
    private String indirizzoSede;
    private String capSede;
    private String provSede;
    private String descrizione;
    private int telSede;
    private String email;
    private String survey;
    private String emergenza;

    @Basic
    @Column(name= "survey")
    public String getSurvey() {
        return survey;
    }

    public void setSurvey(String suvery) {
        this.survey = suvery;
    }

    @Id
    @Column(name = "ID_Sede")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nome_sede")
    public String getNomeSede() {
        return nomeSede;
    }

    public void setNomeSede(String nomeSede) {
        this.nomeSede = nomeSede;
    }

    @Basic
    @Column(name = "indirizzo_sede")
    public String getIndirizzoSede() {
        return indirizzoSede;
    }

    public void setIndirizzoSede(String indirizzoSede) {
        this.indirizzoSede = indirizzoSede;
    }

    @Basic
    @Column(name = "cap_sede")
    public String getCapSede() {
        return capSede;
    }

    public void setCapSede(String capSede) {
        this.capSede = capSede;
    }

    @Basic
    @Column(name = "prov_sede")
    public String getProvSede() {
        return provSede;
    }

    public void setProvSede(String provSede) {
        this.provSede = provSede;
    }

    @Basic
    @Column(name = "descrizione")
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    @Basic
    @Column(name = "tel_sede")
    public int getTelSede() {
        return telSede;
    }

    public void setTelSede(int telSede) {
        this.telSede = telSede;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sede)) return false;
        Sede sede = (Sede) o;
        return telSede == sede.telSede &&
                Objects.equals(id, sede.id) &&
                Objects.equals(nomeSede, sede.nomeSede) &&
                Objects.equals(indirizzoSede, sede.indirizzoSede) &&
                Objects.equals(capSede, sede.capSede) &&
                Objects.equals(provSede, sede.provSede) &&
                Objects.equals(descrizione, sede.descrizione) &&
                Objects.equals(email, sede.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nomeSede, indirizzoSede, capSede, provSede, descrizione, telSede, email);
    }

    @Basic
    @Column(name="emergenza")
    public String getEmergenza() {
        return emergenza;
    }

    public void setEmergenza(String emergenza) {
        this.emergenza = emergenza;
    }
}
