package com.unicam.anavis.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "storico_donazione", schema = "uni_project_ids")
@NamedQueries({
        @NamedQuery(name = "StoricoDonazione.findByUtenteId", query = "SELECT s FROM StoricoDonazione s WHERE s.utenteId = :id"),
        @NamedQuery(name = "StoricoDonazione.findBySedeId", query = "SELECT s FROM StoricoDonazione s WHERE s.sedeIdSede = :id"),
})
public class StoricoDonazione {
    private String id;
    private String gruppoSanguinio;
    private String dataDepositoSangue;
    private String patologie;
    private String descrizioneAnomalie;
    private String lvGlobuli;
    private String lvPiastrine;
    private String quantitaSangueDonato;
    private String utenteId;
    private String sedeIdSede;
    private Date time;
    private Integer duration;
    private String risposteQuestionario;

    @Basic
    @Column(name="RISPOSTE_QUESTIONARIO")
    public String getRisposteQuestionario() {
        return risposteQuestionario;
    }

    public void setRisposteQuestionario(String risposteQuestionario) {
        this.risposteQuestionario = risposteQuestionario;
    }

    @Id
    @Column(name = "id_storico")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "gruppo_sanguinio")
    public String getGruppoSanguinio() {
        return gruppoSanguinio;
    }

    public void setGruppoSanguinio(String gruppoSanguinio) {
        this.gruppoSanguinio = gruppoSanguinio;
    }

    @Basic
    @Column(name = "data_deposito_sangue")
    public String getDataDepositoSangue() {
        return dataDepositoSangue;
    }

    public void setDataDepositoSangue(String dataDepositoSangue) {
        this.dataDepositoSangue = dataDepositoSangue;
    }

    @Basic
    @Column(name = "patologie")
    public String getPatologie() {
        return patologie;
    }

    public void setPatologie(String patologie) {
        this.patologie = patologie;
    }

    @Basic
    @Column(name = "descrizione_anomalie")
    public String getDescrizioneAnomalie() {
        return descrizioneAnomalie;
    }

    public void setDescrizioneAnomalie(String descrizioneAnomalie) {
        this.descrizioneAnomalie = descrizioneAnomalie;
    }

    @Basic
    @Column(name = "lv_globuli")
    public String getLvGlobuli() {
        return lvGlobuli;
    }

    public void setLvGlobuli(String lvGlobuli) {
        this.lvGlobuli = lvGlobuli;
    }

    @Basic
    @Column(name = "lv_piastrine")
    public String getLvPiastrine() {
        return lvPiastrine;
    }

    public void setLvPiastrine(String lvPiastrine) {
        this.lvPiastrine = lvPiastrine;
    }

    @Basic
    @Column(name = "quantita_sangue_donato")
    public String getQuantitaSangueDonato() {
        return quantitaSangueDonato;
    }

    public void setQuantitaSangueDonato(String quantitaSangueDonato) {
        this.quantitaSangueDonato = quantitaSangueDonato;
    }

    @Basic
    @Column(name = "UTENTE_ID")
    public String getUtenteId() {
        return utenteId;
    }

    public void setUtenteId(String utenteId) {
        this.utenteId = utenteId;
    }

    @Basic
    @Column(name = "sede_ID_Sede")
    public String getSedeIdSede() {
        return sedeIdSede;
    }

    public void setSedeIdSede(String sedeIdSede) {
        this.sedeIdSede = sedeIdSede;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoricoDonazione that = (StoricoDonazione) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (gruppoSanguinio != null ? !gruppoSanguinio.equals(that.gruppoSanguinio) : that.gruppoSanguinio != null)
            return false;
        if (dataDepositoSangue != null ? !dataDepositoSangue.equals(that.dataDepositoSangue) : that.dataDepositoSangue != null)
            return false;
        if (patologie != null ? !patologie.equals(that.patologie) : that.patologie != null) return false;
        if (descrizioneAnomalie != null ? !descrizioneAnomalie.equals(that.descrizioneAnomalie) : that.descrizioneAnomalie != null)
            return false;
        if (lvGlobuli != null ? !lvGlobuli.equals(that.lvGlobuli) : that.lvGlobuli != null) return false;
        if (lvPiastrine != null ? !lvPiastrine.equals(that.lvPiastrine) : that.lvPiastrine != null) return false;
        if (quantitaSangueDonato != null ? !quantitaSangueDonato.equals(that.quantitaSangueDonato) : that.quantitaSangueDonato != null)
            return false;
        if (utenteId != null ? !utenteId.equals(that.utenteId) : that.utenteId != null) return false;
        if (sedeIdSede != null ? !sedeIdSede.equals(that.sedeIdSede) : that.sedeIdSede != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (gruppoSanguinio != null ? gruppoSanguinio.hashCode() : 0);
        result = 31 * result + (dataDepositoSangue != null ? dataDepositoSangue.hashCode() : 0);
        result = 31 * result + (patologie != null ? patologie.hashCode() : 0);
        result = 31 * result + (descrizioneAnomalie != null ? descrizioneAnomalie.hashCode() : 0);
        result = 31 * result + (lvGlobuli != null ? lvGlobuli.hashCode() : 0);
        result = 31 * result + (lvPiastrine != null ? lvPiastrine.hashCode() : 0);
        result = 31 * result + (quantitaSangueDonato != null ? quantitaSangueDonato.hashCode() : 0);
        result = 31 * result + (utenteId != null ? utenteId.hashCode() : 0);
        result = 31 * result + (sedeIdSede != null ? sedeIdSede.hashCode() : 0);
        return result;
    }



    @Basic
    @Column(name = "duration")
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }



    @Basic
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
