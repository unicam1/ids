package com.unicam.anavis.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import java.sql.Date;

@Entity
@SecondaryTable(name = "dati_personali", pkJoinColumns = @PrimaryKeyJoinColumn(name = "ID"))
@NamedQueries({
        @NamedQuery(name = "Utente.findByUsername", query = "SELECT u FROM Utente u WHERE u.username = :id"),
        @NamedQuery(name = "Utente.findBySedeRuolo", query = "SELECT u FROM Utente u, Dipendente d WHERE u.id = d.utenteId AND d.sedeIdSede = :idSede AND u.ruolo = :ruolo"),
})
public class Utente {
    //TABELLA DATI PERSONALI
    private String identifierCode;
    private String name;
    private String email;
    private String cognome;
    private String genere;
    private Date dataNascita;
    private String cittaNascita;
    private String provinciaNascita;
    private String statoNascita;
    private String citta;
    private String provincia;
    private String stato;
    private String numeroTelefono;
    private String numeroTelefonoAlternativo;
    private String indirizzo;
    private String numeroCivico;
    private String cap;
    private String statoDomicilio;
    private String provinciaDomicilio;
    private String cittaDomicilio;
    private String indirizzoDomicilio;
    private String numeroCivicoDomicilio;
    private String capDomicilio;
    // TABELLA CUSTOMER
    private String id;
    private String username;
    private String password;
    private String salt;
    private String ruolo;
    private Integer statoUtente;


    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "USERNAME")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "SALT")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Basic
    @Column(name = "RUOLO")
    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    @Basic
    @Column(name = "STATO_UTENTE")
    public Integer getStatoUtente() {
        return statoUtente;
    }

    public void setStatoUtente(Integer statoUtente) {
        this.statoUtente = statoUtente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Utente utente = (Utente) o;

        if (id != null ? !id.equals(utente.id) : utente.id != null) return false;
        if (username != null ? !username.equals(utente.username) : utente.username != null) return false;
        if (password != null ? !password.equals(utente.password) : utente.password != null) return false;
        if (ruolo != null ? !ruolo.equals(utente.ruolo) : utente.ruolo != null) return false;
        if (statoUtente != null ? !statoUtente.equals(utente.statoUtente) : utente.statoUtente != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (ruolo != null ? ruolo.hashCode() : 0);
        result = 31 * result + (statoUtente != null ? statoUtente.hashCode() : 0);
        return result;
    }


    @Basic
    @Column(name = "IDENTIFIER_CODE", table = "dati_personali")
    public String getIdentifierCode() {
        return identifierCode;
    }

    public void setIdentifierCode(String identifierCode) {
        this.identifierCode = identifierCode;
    }

    @Basic
    @Column(name = "NAME", table = "dati_personali")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "EMAIL", table = "dati_personali")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "COGNOME", table = "dati_personali")
    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    @Basic
    @Column(name = "GENERE", table = "dati_personali")
    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    @Basic
    @Column(name = "DATA_NASCITA", table = "dati_personali")
    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    @Basic
    @Column(name = "CITTA_NASCITA", table = "dati_personali")
    public String getCittaNascita() {
        return cittaNascita;
    }

    public void setCittaNascita(String cittaNascita) {
        this.cittaNascita = cittaNascita;
    }

    @Basic
    @Column(name = "PROVINCIA_NASCITA", table = "dati_personali")
    public String getProvinciaNascita() {
        return provinciaNascita;
    }

    public void setProvinciaNascita(String provinciaNascita) {
        this.provinciaNascita = provinciaNascita;
    }

    @Basic
    @Column(name = "STATO_NASCITA", table = "dati_personali")
    public String getStatoNascita() {
        return statoNascita;
    }

    public void setStatoNascita(String statoNascita) {
        this.statoNascita = statoNascita;
    }

    @Basic
    @Column(name = "CITTA", table = "dati_personali")
    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    @Basic
    @Column(name = "PROVINCIA", table = "dati_personali")
    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @Basic
    @Column(name = "STATO", table = "dati_personali")
    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    @Basic
    @Column(name = "NUMERO_TELEFONO", table = "dati_personali")
    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    @Basic
    @Column(name = "NUMERO_TELEFONO_ALTERNATIVO", table = "dati_personali")
    public String getNumeroTelefonoAlternativo() {
        return numeroTelefonoAlternativo;
    }

    public void setNumeroTelefonoAlternativo(String numeroTelefonoAlternativo) {
        this.numeroTelefonoAlternativo = numeroTelefonoAlternativo;
    }

    @Basic
    @Column(name = "INDIRIZZO", table = "dati_personali")
    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    @Basic
    @Column(name = "NUMERO_CIVICO", table = "dati_personali")
    public String getNumeroCivico() {
        return numeroCivico;
    }

    public void setNumeroCivico(String numeroCivico) {
        this.numeroCivico = numeroCivico;
    }

    @Basic
    @Column(name = "CAP", table = "dati_personali")
    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    @Basic
    @Column(name = "STATO_DOMICILIO", table = "dati_personali")
    public String getStatoDomicilio() {
        return statoDomicilio;
    }

    public void setStatoDomicilio(String statoDomicilio) {
        this.statoDomicilio = statoDomicilio;
    }

    @Basic
    @Column(name = "PROVINCIA_DOMICILIO", table = "dati_personali")
    public String getProvinciaDomicilio() {
        return provinciaDomicilio;
    }

    public void setProvinciaDomicilio(String provinciaDomicilio) {
        this.provinciaDomicilio = provinciaDomicilio;
    }

    @Basic
    @Column(name = "CITTA_DOMICILIO", table = "dati_personali")
    public String getCittaDomicilio() {
        return cittaDomicilio;
    }

    public void setCittaDomicilio(String cittaDomicilio) {
        this.cittaDomicilio = cittaDomicilio;
    }

    @Basic
    @Column(name = "INDIRIZZO_DOMICILIO", table = "dati_personali")
    public String getIndirizzoDomicilio() {
        return indirizzoDomicilio;
    }

    public void setIndirizzoDomicilio(String indirizzoDomicilio) {
        this.indirizzoDomicilio = indirizzoDomicilio;
    }

    @Basic
    @Column(name = "NUMERO_CIVICO_DOMICILIO", table = "dati_personali")
    public String getNumeroCivicoDomicilio() {
        return numeroCivicoDomicilio;
    }

    public void setNumeroCivicoDomicilio(String numeroCivicoDomicilio) {
        this.numeroCivicoDomicilio = numeroCivicoDomicilio;
    }

    @Basic
    @Column(name = "CAP_DOMICILIO", table = "dati_personali")
    public String getCapDomicilio() {
        return capDomicilio;
    }

    public void setCapDomicilio(String capDomicilio) {
        this.capDomicilio = capDomicilio;
    }

}
