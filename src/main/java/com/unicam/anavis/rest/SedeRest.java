/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.rest;

import com.unicam.anavis.channel.SedeChannel;
import com.unicam.anavis.mapper.dto.SedeDto;
import com.unicam.util.ProfiledUser;
import com.unicam.util.Utils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/sede")
@Tag(name = "Servizi utente")
public class SedeRest {


    @EJB
    private SedeChannel channel;

    @POST
    @Operation(summary = "Crea sede", description = "Permette la creazione di una sede ")
    @Produces(MediaType.APPLICATION_JSON)
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response create(@Context HttpServletRequest req, SedeDto sedeDto) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.create(profiledUser, sedeDto));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @PUT
    @Operation(summary = "Modifica sede", description = "Permette la modifica di una sede ")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response edit(@Context HttpServletRequest req, SedeDto sedeDto, @PathParam("id") String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.edit(profiledUser, id, sedeDto));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @PUT
    @Operation(summary = "Modifica questionario per la sede", description = "Permette la modifica di una sede ")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/survey")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response editSurvey(@Context HttpServletRequest req, SedeDto sedeDto, @PathParam("id") String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.editSurvey(profiledUser, id, sedeDto));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @GET
    @Operation(summary = "Recupera una sede")
    @Path("/{id}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@Context HttpServletRequest req, @PathParam("id") String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.find(profiledUser, id));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @DELETE
    @Operation(summary = "Modifica sede", description = "Permette la modifica di una sede ")
    @Path("/{id}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@Context HttpServletRequest req,@PathParam("id") String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            channel.delete(profiledUser, id);
            rb = Utils.buildResponse(true);
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @GET
    @PermitAll
    @Operation(summary = "Lista delle sedi", description = "Permette la modifica di una sede ")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@Context HttpServletRequest req) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.findAll(profiledUser));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

}
