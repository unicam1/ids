/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.rest;

import com.unicam.anavis.channel.StoricoDonazioneChannel;
import com.unicam.anavis.mapper.dto.StoricoDonazioneDto;
import com.unicam.util.ProfiledUser;
import com.unicam.util.Utils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/storicodonazione")
@Tag(name = "Servizi utente")
public class StoricoDonazioneRest {


    @EJB
    private StoricoDonazioneChannel channel;

    @POST
    @Operation(summary = "Crea StoricoDonazione", description = "Permette la creazione di una StoricoDonazione ")
    @Produces(MediaType.APPLICATION_JSON)
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response create(@Context HttpServletRequest req, StoricoDonazioneDto StoricoDonazioneDto) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.create(profiledUser, StoricoDonazioneDto));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @PUT
    @Operation(summary = "Modifica StoricoDonazione", description = "Permette la modifica di una StoricoDonazione ")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response edit(@Context HttpServletRequest req, StoricoDonazioneDto StoricoDonazioneDto, @PathParam("id") String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.edit(profiledUser, id, StoricoDonazioneDto));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @DELETE
    @Operation(summary = "Modifica StoricoDonazione", description = "Permette la modifica di una StoricoDonazione ")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response delete(@Context HttpServletRequest req,@PathParam("id") String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            channel.delete(profiledUser, id);
            rb = Utils.buildResponse(true);
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @GET
    @Operation(summary = "Modifica StoricoDonazione", description = "Permette la modifica di una StoricoDonazione ")
    @Produces(MediaType.APPLICATION_JSON)
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response edit(@Context HttpServletRequest req) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.findAll(profiledUser));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }
    @GET
    @Operation(summary = "Modifica StoricoDonazione", description = "Permette la modifica di una StoricoDonazione ")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response edit(@Context HttpServletRequest req, @PathParam("id")String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.findAll(profiledUser, id));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }
    @GET
    @Operation(summary = "Modifica StoricoDonazione", description = "Permette la modifica di una StoricoDonazione ")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/detail/{id}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response detailedWithUser(@Context HttpServletRequest req, @PathParam("id")String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.findAllWithUser(profiledUser, id));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

}
