/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.anavis.rest;

import com.unicam.anavis.channel.UserServiceChannel;
import com.unicam.anavis.mapper.dto.UtenteDto;
import com.unicam.util.ProfiledUser;
import com.unicam.util.Utils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/users")
@Tag(name = "Servizi utente")
public class UserRest {

    private static final Logger LOG = Logger.getLogger(UserRest.class.getName());
    @EJB
    private UserServiceChannel channel;

    @GET
    @PermitAll
    @Operation(summary = "Login", description = "Restituisce il token da utilizzare successivamente per tutte le chiamate")
    @Path("/signin")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponse(responseCode = "200", description = "JWT token for authorization", content = @Content(mediaType = "application/json"))
    @SecurityRequirement(name = "BasicAuth", scopes = "[]")
    public Response signIn(@Context HttpServletRequest req) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.signIn(req, profiledUser));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error", ex);
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @POST
    @PermitAll
    @Operation(summary = "Registrazione", description = "Restituisce l'id dell'utente creato")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiResponse(responseCode = "200", description = "JWT token for authorization", content = @Content(mediaType = "application/json"))
    @SecurityRequirement(name = "BasicAuth", scopes = "[]")
    public Response registerUser(@Context HttpServletRequest req, UtenteDto utenteDto) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.register(profiledUser, utenteDto));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error", ex);
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }


    @POST
    @Operation(summary = "Registrazione", description = "Restituisce l'id dell'utente creato")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    @Path("/{idSede}")
    public Response registerUser(@Context HttpServletRequest req, UtenteDto utenteDto, @PathParam("idSede") String idSede) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.register(profiledUser, utenteDto, idSede));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error", ex);
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @PUT
    @Operation(summary = "Modifica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response Edit(@Context HttpServletRequest req, UtenteDto utenteDto, @PathParam("id") String id) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.edit(profiledUser, utenteDto, id));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error", ex);
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @GET
    @Operation(summary = "Ottiene gli utenti filtrati per sede e per ruolo")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{role}/{idSede}")
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response filteredUser(@Context HttpServletRequest req, @PathParam("role") String role, @PathParam("idSede") String idSede) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.findAll(profiledUser, idSede, role));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }

    @GET
    @Operation(summary = "Ottengo informazioni sull'utente loggato")
    @Produces(MediaType.APPLICATION_JSON)
    @SecurityRequirement(name = "Bearer", scopes = "[]")
    public Response myInfo(@Context HttpServletRequest req) {
        Response.ResponseBuilder rb;
        ProfiledUser profiledUser = Utils.getIdentifierFromHeader(req);
        try {
            rb = Utils.buildResponse(channel.myInfo(profiledUser));
        } catch (Exception ex) {
            rb = Utils.buildResponseError(Response.Status.BAD_REQUEST, ex, profiledUser);
        }
        return rb.build();
    }
}
