package com.unicam.anavis.security;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Servlet Filter implementation class CORSFilter
 */
public class CorsFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(CorsFilter.class.getName());

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		LOG.info("CORSFilter HTTP Request: " + request.getMethod());
		HttpServletResponse resp = (HttpServletResponse) servletResponse;
		// For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS
		if (request.getMethod().equals("OPTIONS")) {
			// Authorize (allow) all domains to consume the content
			resp.addHeader("Access-Control-Allow-Headers", "Authorization, Bearer");
			resp.setStatus(HttpServletResponse.SC_ACCEPTED);
			return;
		}
		// pass the request along the filter chain
		chain.doFilter(request, servletResponse);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
}