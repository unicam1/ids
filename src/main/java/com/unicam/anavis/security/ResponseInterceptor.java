package com.unicam.anavis.security;

import com.unicam.security.SecurityUtils;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.util.logging.Logger;

/**
 * This filter verify the access permissions for a user based on username and
 * passowrd provided in request
 */
@Provider
@Priority(Priorities.HEADER_DECORATOR)
public class ResponseInterceptor implements javax.ws.rs.container.ContainerResponseFilter {

    private static final Logger LOG = Logger.getLogger(ResponseInterceptor.class.getName());
    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        SecurityUtils securityUtils = new SecurityUtils();
        securityUtils.handleResponseBuilder(requestContext, responseContext, LOG);
    }


}
