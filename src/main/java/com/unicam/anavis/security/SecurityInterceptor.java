package com.unicam.anavis.security;

import com.unicam.security.SecurityUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.util.logging.Logger;

/**
 * This filter verify the access permissions for a user based on username and
 * passowrd provided in request
 */
@Provider
public class SecurityInterceptor implements javax.ws.rs.container.ContainerRequestFilter {

    private static final Logger LOG = Logger.getLogger(SecurityInterceptor.class.getName());
    @Context
    private ResourceInfo resourceInfo;
    @Context
    private HttpServletRequest sr;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        SecurityUtils securityUtils = new SecurityUtils();
        securityUtils.handleLoginPermission(requestContext, resourceInfo, sr, LOG, false);
    }


}
