/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.application.config;

import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.security.SecuritySchemes;
import io.swagger.v3.oas.annotations.servers.Server;

import javax.ws.rs.core.Application;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author mauro
 */
@javax.ws.rs.ApplicationPath("api/v1")
@OpenAPIDefinition(info = @Info(title = "OpenAPI di unicam", version = "0.1", description = "Descrizione", contact = @Contact(url = "http://unicam.it", name = "Mauro", email = "mauroad.mazzocchetti@studenti.unicam.it")), servers = {
        @Server(url = "http://localhost:8080/anavis-1.0.0")})

@SecuritySchemes(value = {
        @SecurityScheme(name = "BasicAuth", type = SecuritySchemeType.HTTP, in = SecuritySchemeIn.HEADER, scheme = "basic"),
        @SecurityScheme(name = "Bearer", type = SecuritySchemeType.HTTP, in = SecuritySchemeIn.HEADER, scheme = "bearer", bearerFormat = "JWT")

})
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        resources.add(OpenApiResource.class);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically populated
     * with all resources defined in the project. If required, comment out calling
     * this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.unicam.anavis.security.SecurityInterceptor.class);
        resources.add(com.unicam.anavis.security.ResponseInterceptor.class);
        resources.add(com.unicam.anavis.rest.UserRest.class);
        resources.add(com.unicam.anavis.rest.SedeRest.class);
        resources.add(com.unicam.anavis.rest.StoricoDonazioneRest.class);
    }

}
