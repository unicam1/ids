package com.unicam.exception;

import com.unicam.util.ProfiledUser;

public class Unicam4Exception extends Exception {

    private ProfiledUser profiledUser;
    private String errorCode;
    private ExceptionData exceptionData;

    public Unicam4Exception(String message, ProfiledUser profiledUser, String errorCode) {
        super(message);
        exceptionData = new ExceptionData();
        this.profiledUser = profiledUser;
        this.errorCode = errorCode;
        exceptionData.setErrorCode(errorCode);
        exceptionData.setMessage(message);
        if (profiledUser != null) {
            exceptionData.setUuid(profiledUser.getUuid());
        }else{
            exceptionData.setMessage("Wrong path or incorrect parameter in the path param");
        }
    }

    public Unicam4Exception(String message) {
        super(message);
        exceptionData = new ExceptionData();
        exceptionData.setMessage(message);
    }

    public ProfiledUser getProfiledUser() {
        return profiledUser;
    }

    public void setProfiledUser(ProfiledUser profiledUser) {
        this.profiledUser = profiledUser;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ExceptionData getExceptionData() {
        return exceptionData;
    }

}
