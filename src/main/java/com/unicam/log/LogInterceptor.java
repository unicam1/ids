package com.unicam.log;

import com.unicam.exception.Unicam4Exception;
import com.unicam.util.ErrorConst;
import com.unicam.util.ProfiledUser;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogInterceptor {

    protected static final long MILLIS_FOR_WARNING = 45000;

    private ProfiledUser recuperaProfiledUser(Object... objects) {
        if (objects != null) {
            for (Object o : objects) {
                if (o != null && o.getClass() != null && o.getClass().getSimpleName().toLowerCase().contains("profileduser") && ProfiledUser.class.getName().equals(o.getClass().getName())) {
                    return (ProfiledUser) o;
                }
            }
        }
        return null;
    }


    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        Level logLevel = Level.INFO;
        final Logger log = Logger.getLogger(context.getTarget().getClass().getName());
        long start = System.currentTimeMillis();
        ProfiledUser profiledUser = null;
        String uuid = "NONE";
        try {

            // Non è possibile passare null nell'ultimo parametro della funzione
            // logp, si genererebbe un eccezione
            profiledUser = recuperaProfiledUser(context.getParameters());
            if (profiledUser != null) {
                uuid = profiledUser.getUuid();
            }
            log.logp(logLevel, context.getTarget().getClass().getName(), context.getMethod().getName() + " - " + uuid, "START - " + context.getMethod().getName() + " - INPUT: " + LogUtils.dataLogTag("INPUT", context.getParameters()), new Object[0]);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error in log input method", e);
        }

        Object output = null;
        try {
            // Chiamata al metodo
            output = context.proceed();
        } catch (Unicam4Exception e) {
            long end = System.currentTimeMillis() - start;
            log.logp(Level.SEVERE, context.getTarget().getClass().getName(), context.getMethod().getName() + " - " + uuid, "Unicam4Exception [" + end + "ms] - " + context.getMethod().getName());
            throw e;
        } catch (Exception e) {
            long end = System.currentTimeMillis() - start;
            log.logp(Level.SEVERE, context.getTarget().getClass().getName(), context.getMethod().getName() + " - " + uuid, "ERROR IN DATA SERIALIZATION PROCESS [" + end + "ms] - " + context.getMethod().getName() + " - OUTPUT: " + e.getMessage() + LogUtils.dataLogTag("OUTPUT", output), e);
            throw new Unicam4Exception(e.getMessage(), profiledUser, ErrorConst.GENERIC_ERROR);
        }

        // Log per le prestazioni dei metodi
        long end = System.currentTimeMillis() - start;
        log.logp(logLevel, context.getTarget().getClass().getName(), context.getMethod().getName() + " - " + uuid, "END [" + end + "ms] - " + context.getMethod().getName() + " - OUTPUT: " + LogUtils.dataLogTag("OUTPUT", output));
        return output;
    }

}
