package com.unicam.log;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

@SuppressWarnings("restriction")
public class LogToStringStyle extends RecursiveToStringStyle {

    private static final long serialVersionUID = 1L;
    public static final ToStringStyle LOG_PREFIX_STYLE = new LogToStringStyle();

    public LogToStringStyle() {
        super();
        this.setNullText(null);
        this.setUseShortClassName(true);
        this.setUseIdentityHashCode(false);
        this.setDefaultFullDetail(true);
        this.setArrayEnd("]");
        this.setArrayStart("[");
        this.setArraySeparator("");
        this.setContentStart("{");
        this.setContentEnd("}");
        this.setFieldNameValueSeparator(":");
        this.setFieldSeparatorAtEnd(true);
        this.setFieldSeparatorAtStart(false);
        this.setFieldSeparator(",");
    }

    /**
     * <p>
     * Append to the <code>toString</code> an <code>Object</code> value,
     * printing the full <code>toString</code> of the <code>Object</code> passed
     * in.
     * </p>
     *
     * @param buffer the <code>StringBuffer</code> to populate
     * @param fieldName the field name
     * @param value the value to add to the <code>toString</code>
     * @param fullDetail <code>true</code> for detail, <code>false</code> for
     * summary info, <code>null</code> for style decides
     */
    @Override
    public void append(StringBuffer buffer, String fieldName, Object value, Boolean fullDetail) {

        if (value == null) {
            this.setFieldSeparator("");
            this.setFieldNameValueSeparator("");
            appendFieldStart(buffer, "");
            appendNullText(buffer, fieldName);
        } else {
            this.setFieldSeparator(",");
            this.setFieldNameValueSeparator("=");
            appendFieldStart(buffer, fieldName);
            appendInternal(buffer, fieldName, value, isFullDetail(fullDetail));
        }
        appendFieldEnd(buffer, fieldName);
    }

    @Override
    public void appendDetail(final StringBuffer buffer, final String fieldName, final Object value) {

        if (accept(value.getClass())) {
            buffer.append(ReflectionToStringBuilder.toString(value, new LogToStringStyle()));
        } else if (BigDecimal.class.equals(value.getClass())) {
            super.appendDetail(buffer, fieldName, ((BigDecimal) value).toPlainString());
        } else if (String.class.equals(value.getClass())) {
            if (fieldName != null && (fieldName.equalsIgnoreCase("otp") || fieldName.equalsIgnoreCase("pin") || fieldName.equalsIgnoreCase("psw") || fieldName.toLowerCase().contains("pwd") || fieldName.toLowerCase().contains("password") || fieldName.toLowerCase().contains("pass"))) {
                super.appendDetail(buffer, fieldName, StringUtils.hidePassword((String) value));
            } else {
                super.appendDetail(buffer, fieldName, value);
            }
        } else {
            super.appendDetail(buffer, fieldName, value);
        }
    }

    @Override
    public void appendStart(StringBuffer buffer, Object object) {
        if (object != null) {
            appendClassName(buffer, object);
            appendIdentityHashCode(buffer, object);
            appendContentStart(buffer);
            if (isFieldSeparatorAtStart()) {
                appendFieldSeparator(buffer);
            }
        }
    }

    @Override
    protected void appendDetail(final StringBuffer buffer, final String fieldName, final byte[] array) {
        buffer.append(getArrayStart());
        if (array != null) {
            buffer.append("Documento presente, dimensione: " + ((byte[]) array).length);
        } else {
            buffer.append("Documento non presente");
        }
        buffer.append(getArrayEnd());
    }

    @Override
    public void append(final StringBuffer buffer, final String fieldName, final byte[] array, final Boolean fullDetail) {
        appendFieldStart(buffer, fieldName);

        if (array == null) {
            appendNullText(buffer, fieldName);

        } else if (isFullDetail(fullDetail)) {
            appendDetail(buffer, fieldName, array);

        } else {
            appendSummary(buffer, fieldName, array);
        }

        appendFieldEnd(buffer, fieldName);
    }

    @Override
    protected boolean accept(final Class<?> clazz) {
        return !ClassUtils.isPrimitiveWrapper(clazz) && !String.class.equals(clazz) && !BigDecimal.class.equals(clazz);
    }
}
