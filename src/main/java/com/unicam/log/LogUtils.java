package com.unicam.log;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

/**
 * @author mmazzocchetti
 */
public class LogUtils {

    protected static final long MILLIS_FOR_WARNING = 45000;

    /**
     * Un istanza di classe creata attraverso questo metodo può avere un
     * costruttore che prende in ingresso delle variabili.
     *
     * Tutti i metodi richiamati da questa istanza della classe scriveranno nei
     * log i seguenti dati:
     *
     * <ul>
     * <li>Inizio</li>
     * <li>Fine</li>
     * <li>Tempi per performance</li>
     * <li>Input</li>
     * <li>Output</li>
     * <li>Eventuali eccezioni</li>
     * </ul>
     *
     * @param classe
     * @return Ritorna un istanza della classe passata in ingresso
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     */
//        public static Object logclass(Class<?> classe, Object... constructors) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
//                ProxyFactory f = new ProxyFactory();
//                f.setSuperclass(classe);
//                final Logger log = Logger.getLogger(classe.getName());
//                Class<?> c = f.createClass();
//
//                // Creo un method handler personalizzato
//                MethodHandler mi = new LogMethodHandler(log, constructors);
//
//                Object instance = null;
//                if (constructors != null) {
//                        Class<?>[] parameters = new Class[constructors.length];
//                        for (int i = 0; i < constructors.length; i++) {
//                                parameters[i] = constructors[i].getClass();
//                        }
//                        instance = c.getDeclaredConstructor(parameters).newInstance(constructors);
//                } else {
//                        instance = c.newInstance();
//                }
//                for (Method m : instance.getClass().getMethods()) {
//                        if (m.getName().equalsIgnoreCase("setHandler")) {
//                                m.invoke(instance, mi);
//                                break;
//                        }
//                }
//                return instance;
//        }

    /**
     * Ritorna una stringa, che contiene tutti i valori passati in ingresso,
     * divisi in tag xml
     *
     * @param tag       Valore con cui viene valorizzato il tag xml con cui vengono
     *                  parsati i valori
     * @param inputList
     * @return
     */
    @SafeVarargs
    @SuppressWarnings("unchecked")
    public static <T> String dataLogTag(String tag, T... inputList) {
//        return "Test";
        if (tag == null) {
            tag = "DATAN";
        }
        StringBuffer log = new StringBuffer(" <VarArgsLog>");
        Integer element = 1;
        for (T input : inputList) {
            if (input != null) {
                log.append(" <" + tag + element + ">");
                if (input instanceof List) {
                    List<T> list = (List<T>) input;
                    for (T i : list) {
                        log.append("[" + new ReflectionToStringBuilder(i, new LogToStringStyle()).toString() + "]");
                    }
                } else if (input instanceof String) {
                    log.append((String) input);
                } else if (input instanceof ResultSet) {
                    log.append("Result set passato in input");
                } else if (input instanceof BigDecimal) {
                    log.append(((BigDecimal) input).toPlainString());
                } else if (input instanceof byte[][]) {
                    log.append("File presenti " + ((byte[][]) input).length + ", dimensioni: ");
                    for (byte[] i : ((byte[][]) input)) {
                        if (i != null) {
                            log.append(" -> " + i.length);
                        } else {
                            log.append(" -> null");
                        }
                    }
                } else if (input instanceof byte[]) {
                    log.append("File presente, dimensione: " + ((byte[]) input).length);
                } else if (input instanceof Connection) {
                    log.append("Connessione");
                } else {
                    try {
                        log.append(new ReflectionToStringBuilder(input, new LogToStringStyle()).toString());
                    } catch (Exception e) {
                        log.append("WARNING - Errore nel recupero dati per la classe " + input.getClass().getName());
                    }
                }
                // Chiudo il tag
                log.append(" </" + tag + element++ + "> ");
            }
        }
        log.append(" </VarArgsLog>");
        return StringUtils.inlineString(log);
    }

    /**
     * Ritorna una stringa, che contiene tutti i valori passati in ingresso,
     * divisi in tag xml
     *
     * @param inputList
     * @return
     */
    @SafeVarargs
    public static <T> String dataLog(T... inputList) {
        return dataLogTag("Data", inputList);
    }

}
