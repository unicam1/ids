package com.unicam.log;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

    // private static final Logger logger = Logger.getLogger(StringUtils.class.getName());
    public static String inlineString(String str) {
        return str.replaceAll("[\n\r]", "").trim();
    }

    public static String inlineString(StringBuilder str) {
        return str.toString().replaceAll("[\n\r]", "").trim();
    }

    public static String hidePassword(String value) {
        if (value == null || value.length() < 3) {
            return value;
        } else {
            return value.substring(0, 2) + StringUtils.leftPad("", value.length() - 2, "*");
        }
    }

    public static String inlineString(StringBuffer str) {
        return str.toString().replaceAll("[\n\r]", "").trim();
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.equals("");
    }

}
