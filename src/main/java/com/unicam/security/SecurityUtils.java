package com.unicam.security;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unicam.exception.Unicam4Exception;
import com.unicam.log.LogUtils;
import com.unicam.util.Constant;
import com.unicam.util.EncoderDecoder;
import com.unicam.util.ErrorConst;
import com.unicam.util.ProfiledUser;
import com.unicam.util.Utils;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SecurityUtils {
    private static final Response TOKEN_EXPIRED = Response.status(Response.Status.PRECONDITION_FAILED)
            .entity("You cannot access this resource").build();
    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED)
            .entity("You cannot access this resource").build();
    private static final Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN)
            .entity("Access blocked for all users!").build();
    private static final Response ACCESS_ERROR = Response.status(Response.Status.EXPECTATION_FAILED)
            .entity("Access error!").build();


    public void handleResponseBuilder(ContainerRequestContext requestContext, ContainerResponseContext responseContext, Logger LOG) {
        responseContext.getHeaders().add(Constant.AUTHORIZATION_PROPERTY, Utils.getJwtFromHeader(requestContext));
        responseContext.getHeaders().add("Access-Control-Allow-Headers", "*");
        //In caso di errore
        if (responseContext.getStatus() > 299) {
            Object tempError = responseContext.getEntity();
            if (tempError == null) {
                tempError = "Generic error";
            }
            //Genero un eccesione generica con il testo dell'eccezione lanciata
            Unicam4Exception exception = new Unicam4Exception(tempError.toString(), Utils.getIdentifierFromHeader(requestContext), ErrorConst.GENERIC_ERROR);
            ObjectMapper mapper = new ObjectMapper();
            //Restituisco un json
            try {
                responseContext.setEntity(mapper.writeValueAsString(exception.getExceptionData()), null, MediaType.valueOf(MediaType.APPLICATION_JSON));
            } catch (JsonProcessingException e) {
                LOG.severe("Error converting message entity response");
                responseContext.setEntity("Error converting message entity response");
            }
        }

        String logData = "END - " + LogUtils.dataLogTag("METHOD", requestContext.getMethod())
                + LogUtils.dataLogTag("HEADER", responseContext.getHeaders().toString())
                + LogUtils.dataLogTag("BODY", responseContext.getEntity()) + "IDENTIFIER: "
                + Utils.getIdentifierFromHeader(requestContext).getUuid();
        LOG.info(logData);
    }


    public void handleLoginPermission(ContainerRequestContext requestContext, ResourceInfo resourceInfo, HttpServletRequest sr, Logger log, Boolean allowAdmin) {
        String uuid = UUID.randomUUID().toString();
        try {
            Method method = resourceInfo.getResourceMethod();
            String logData = "START - IP: " + sr.getRemoteAddr() + " "
                    + LogUtils.dataLogTag("METHOD", requestContext.getMethod())
                    + LogUtils.dataLogTag("HEADER", requestContext.getHeaders().toString()) + "IDENTIFIER: " + uuid;
            log.info(logData);

            // 1. Se è permesso a tutti non ci sono restrizioni
            // FDEV Forse una definizione migliore dello swagger?
            if (!method.isAnnotationPresent(PermitAll.class) && !sr.getRequestURI().contains("openapi.json")) {
                // 2. Metodo non più accessibile
                if (method.isAnnotationPresent(DenyAll.class)) {
                    requestContext.abortWith(ACCESS_FORBIDDEN);
                    return;
                }

                // 3. Recupero l'header
                final MultivaluedMap<String, String> headers = requestContext.getHeaders();

                // 4. Recupero l'auth
                final List<String> authorization = headers.get(Constant.AUTHORIZATION_PROPERTY);

                // 5. Se non presente restituisco accesso negato
                if (authorization == null || authorization.isEmpty()) {
                    requestContext.abortWith(ACCESS_DENIED);
                    return;
                }

                // 6. Recupero username e password encodate
                final String encodedUserPassword = authorization.get(0)
                        .replaceFirst(Constant.AUTHENTICATION_SCHEME_JWT + " ", "");

                // 7. Faccio il decode da JWT
                DecodedJWT usernameAndPassword;
                try {
                    usernameAndPassword = EncoderDecoder.verifyDecodeJwt(encodedUserPassword,
                            sr.getRemoteAddr());
                } catch (TokenExpiredException ek) {
                    requestContext.abortWith(TOKEN_EXPIRED);
                    return;
                }
                ProfiledUser profiledUser = new ProfiledUser();
                profiledUser.setUsername(usernameAndPassword.getClaim(Constant.AUTHORIZATION_JWT_USER).asString());
                profiledUser.setUuid(uuid);
                profiledUser.setUserId(usernameAndPassword.getClaim(Constant.AUTHORIZATION_JWT_CUSTOMER_ID).asString());
                requestContext.setProperty(Constant.AUTHORIZATION_DATA_CLEAR, profiledUser);
                requestContext.setProperty(Constant.AUTHORIZATION_PROPERTY, EncoderDecoder.renewJwt(usernameAndPassword));

            } else {
                // TODO Cambiare non necessito del primo parametro
                ProfiledUser profiledUser = new ProfiledUser();
                profiledUser.setUuid(uuid);
                requestContext.setProperty(Constant.AUTHORIZATION_DATA_CLEAR, profiledUser);
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, "Generic error during the request", ex);
            requestContext.abortWith(ACCESS_ERROR);
            return;
        }
    }
}
