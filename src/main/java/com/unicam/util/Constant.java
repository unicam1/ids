/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.util;

/**
 * @author mauro
 */
public class Constant {

    final static public String AUTHORIZATION_DATA_CLEAR = "X-AUTH-DATA-CLEAR";
    final static public String AUTHORIZATION_JWT_KEY = System.getProperty("authorization.jwt.key");
    final static public String AUTHORIZATION_JWT_ALLOWED_PATHS = "allowedPaths";
    final static public String AUTHORIZATION_JWT_ALLOWED_GROUPS = "allowedGroups";
    final static public String AUTHORIZATION_JWT_UUID = "uuid";
    final static public String AUTHORIZATION_JWT_CUSTOMER_ID = "customerId";
    final static public String AUTHORIZATION_JWT_ACCOUNT_ID = "customerAccount";
    final static public String AUTHORIZATION_JWT_EXTERNAL_PERSONAL_DATA = "employeeData";
    final static public String AUTHORIZATION_JWT_CUSTOMER_ID_ACCOUNT = "customerIdAccount";
    final static public String AUTHORIZATION_ADMIN_CUSTOM_TOKEN = "admin";
    final static public String AUTHORIZATION_SESSION_TOKEN = "session";
    final static public String AUTHORIZATION_JWT_USER = "username";
    final static public String AUTHORIZATION_PROPERTY = "Authorization";
    public static final String AUTHENTICATION_SCHEME = "Basic";
    public static final String AUTHENTICATION_SCHEME_JWT = "Bearer";
    public static final Integer SALT_LENG = 40;

    public static final String LEVEL_INFO = "INFO";
    public static final String LEVEL_WARNING = "WARNING";
    public static final String LEVEL_SEVERE = "SEVERE";

}
