/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mauro
 */
public class EncoderDecoder {


    private static final Integer ADMIN_MINUTES = 15;
    private static final Integer CUSTOMER_MINUTES = 120;

    // TODO Configure the key from the server parameter, Utilizzerei il naming
    // context [JNDI lookup] e lo si configura nel server inoltre una chiave messa
    // direttamente nel codice

    /**
     * Vanno aggiunti i vari claims che sono i dati che si vogliono tenere nel
     * payload
     *
     * @param user User id
     * @param uuid Uuid univoco per riconoscere la chiamata
     * @param ip   Indirizzo ip del client
     * @return
     */
    public static String generateJwt(String user, String uuid, String ip, String customerId) {
        String output;
        Algorithm algorithm = Algorithm.HMAC256(getSecretKey() + Constant.AUTHORIZATION_JWT_KEY);

        output = JWT.create().withClaim(Constant.AUTHORIZATION_JWT_USER, user)
                .withClaim(Constant.AUTHORIZATION_JWT_CUSTOMER_ID, customerId)
                .withClaim(Constant.AUTHORIZATION_JWT_UUID, uuid)
                .withIssuer(ip).withExpiresAt(DateUtils.addMinutes(new Date(), CUSTOMER_MINUTES)).sign(algorithm);
        return output;
    }

    public static String generateAdminJwt(String user, String uuid, String ip, String customerId, String sessionId) {
        String output;
        Algorithm algorithm = Algorithm.HMAC256(getSecretKey() + Constant.AUTHORIZATION_JWT_KEY);

        output = JWT.create().withClaim(Constant.AUTHORIZATION_JWT_USER, user)
                .withClaim(Constant.AUTHORIZATION_JWT_CUSTOMER_ID, customerId)
                .withClaim(Constant.AUTHORIZATION_ADMIN_CUSTOM_TOKEN, Constant.AUTHORIZATION_ADMIN_CUSTOM_TOKEN)
                .withClaim(Constant.AUTHORIZATION_SESSION_TOKEN, sessionId)
                .withClaim(Constant.AUTHORIZATION_JWT_UUID, uuid)
                .withIssuer(ip).withExpiresAt(DateUtils.addMinutes(new Date(), ADMIN_MINUTES)).sign(algorithm);
        return output;
    }


    public static DecodedJWT verifyDecodeJwtEmail(String jwt) {
        DecodedJWT output;
        Algorithm algorithm = Algorithm.HMAC256(getSecretKey() + Constant.AUTHORIZATION_JWT_KEY);
        output = JWT.require(algorithm).build().verify(jwt);
        return output;
    }


    /**
     * Dato una list di AllowedGroups lo converte in un array di String
     *
     * @param allowedGroups List<String> da convertire
     * @return String[] output
     */
    public static String[] encodeAllowedGroupObjectToStringArray(List<String> allowedGroups) {
        List<String> output = new ArrayList<String>();

        for (int i = 0; i < allowedGroups.size(); i++) {

            output.add(allowedGroups.get(i));

        }
        return output.toArray(new String[0]);
    }

    /**
     * Dato un array di String lo decoda in un List<String>
     *
     * @param allowedGroupsEncoded String[] da convertire
     * @return List<String> output
     */
    public static List<String> decodeStringArrayToAllowedGroupObject(String[] allowedGroupsEncoded) {

        List<String> output = new ArrayList<String>();

        for (int i = 0; i < allowedGroupsEncoded.length; i++) {
            // 1. L'array allowedGroupsEncoded conterrà nell'index 0 il Method e nell'index
            // 1 il Path

            output.add(allowedGroupsEncoded[i]);
        }

        return output;
    }

    /**
     * Dato un JWT lo rinnova
     *
     * @param decodedJWT JWT validato
     * @return String JWT
     */
    public static String renewJwt(DecodedJWT decodedJWT) {
        String output;
        output = generateJwt(decodedJWT.getClaim(Constant.AUTHORIZATION_JWT_USER).asString(),
                decodedJWT.getClaim(Constant.AUTHORIZATION_JWT_UUID).asString(),
                decodedJWT.getIssuer(),
                decodedJWT.getClaim(Constant.AUTHORIZATION_JWT_CUSTOMER_ID).asString());
        return output;
    }

    public static DecodedJWT verifyDecodeJwt(String jwt, String ip) {
        DecodedJWT output;
        Algorithm algorithm = Algorithm.HMAC256(getSecretKey() + Constant.AUTHORIZATION_JWT_KEY);
        output = JWT.require(algorithm).withIssuer(ip).build().verify(jwt);
        return output;
    }

    private static String getSecretKey() {
        return "SOME_SECRET_KEY";
    }
}
