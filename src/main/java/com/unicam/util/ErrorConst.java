/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.util;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


/**
 * @author mauro
 */
public class ErrorConst {

    final static public String ERROR_SIGNIN_CHANNEL = "E001";
    final static public String ERROR_CREATEUSER_MISSINGGROUP = "E002";
    final static public String ERROR_CREATEUSER_MISSINGPERSONALDATA = "E003";
    final static public String ERROR_CREATEUSER_MISSINGPRODUCT = "E004";
    final static public String ERROR_AUTH_NOUSER = "E004";
    final static public String ERROR_AUTH_UNAUTH = "E005";
    final static public String ERROR_AUTH_GROUP = "E006";
    final static public String ERROR_PASSWORDS_DONT_MATCH = "E007";
    final static public String GENERIC_ERROR = "E008";
    final static public String ERROR_CREATEUSER_MISSINGROLE = "E009";
    final static public String ERROR_CREATEUSER_USERNAMENOTAVAIABLE = "E010";

    static public Status getStatusCode(String errorCode) {
        Status output;
        switch (errorCode) {
            case ERROR_AUTH_UNAUTH:
            case ERROR_AUTH_NOUSER:
            case ERROR_AUTH_GROUP:
                output = Status.FORBIDDEN;
                break;
            case ERROR_SIGNIN_CHANNEL:
                output = Status.UNAUTHORIZED;
                break;
            case ERROR_PASSWORDS_DONT_MATCH:
            case ERROR_CREATEUSER_MISSINGGROUP:
            case ERROR_CREATEUSER_MISSINGPERSONALDATA:
                output = Status.BAD_REQUEST;
                break;

            default:
                output = Status.INTERNAL_SERVER_ERROR;
        }
        return output;
    }
}
