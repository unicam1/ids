/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicam.util;

import com.unicam.exception.Unicam4Exception;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mauro
 */
public class Utils {

    public static ProfiledUser getIdentifierFromHeader(ContainerRequestContext requestContext) {
        if (requestContext.getProperty(Constant.AUTHORIZATION_DATA_CLEAR) != null) {
            ProfiledUser completeToken = (ProfiledUser) requestContext.getProperty(Constant.AUTHORIZATION_DATA_CLEAR);
            return completeToken;
        } else {
            return new ProfiledUser();
        }
    }

    public static String getJwtFromHeader(ContainerRequestContext requestContext) {
        Object output = requestContext.getProperty(Constant.AUTHORIZATION_PROPERTY);
        if (output != null) {
            return output.toString();
        } else {
            return null;
        }
    }

    public static ProfiledUser getIdentifierFromHeader(HttpServletRequest req) {
        ProfiledUser completeToken = (ProfiledUser) req.getAttribute(Constant.AUTHORIZATION_DATA_CLEAR);
        return completeToken;
    }

    public static Response.ResponseBuilder buildResponse(Object output) {
        return Response.ok(output);
    }

    public static <E> Response.ResponseBuilder buildResponse(Collection<E> output) {
        return Response.ok(output);
    }

    public static Response.ResponseBuilder buildResponseError(Response.Status status, Exception e, ProfiledUser profiledUser) {
        final Logger log = Logger.getLogger("BuildResponseError");
        Unicam4Exception t = null;
        if (e instanceof Unicam4Exception) {
            t = (Unicam4Exception) e;
        } else {
            t = new Unicam4Exception(e.getMessage(), profiledUser, ErrorConst.GENERIC_ERROR);
        }
        log.logp(Level.SEVERE, "Error", profiledUser.getUuid(), "RESPONSE ERROR ", e);
        return Response.status(ErrorConst.getStatusCode(t.getErrorCode())).entity(t.getExceptionData());
    }

}
